package com.bbva.tata.lib.rmg1;

import java.util.List;

import com.bbva.tata.dto.cuentasjson.AccountJSONDTO;

/**
 * The  interface TATARMG1 class...
 */
public interface TATARMG1 {

	/**
	 * The execute method...
	 */
	int executeCreateDocument(AccountJSONDTO accountJsonDTOIn);
	List<AccountJSONDTO> executeGetDocuments(AccountJSONDTO accountJsonDTOIn);

}
